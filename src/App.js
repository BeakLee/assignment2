const app = require('./App.css');
const localIpUrl = require('local-ip-url');
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src="/safu.jpg" alt=""/>
        <p>
          CEN4083 Webapp Assignment#2
        </p>
        <p>
          VM EC2 IP: {localIpUrl()}
        </p>
        <a
          className="App-link"
          href="https://canvas.fiu.edu/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Return to Canvas
        </a>
      </header>
    </div>
  );
} 

export default App;
